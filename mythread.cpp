#include "mythread.h"
#include <fstream>
#include <QMutexLocker>
#include <QMutex>

QMutex m;

void MyThread::run()
{
    std::fstream f;
    int last = 0;
    int current = 0;

    while(true)
    {
        //std::unique_lock<std::mutex> lck{m};
        //while (!changed)
            //cv.wait(lck);
        QMutexLocker locker(&m);
        f.open("checker.txt");
        if (f.is_open())
        {
            last = current;
            f >> current;
            f.close();
        }
        if (last != current)
        {
            window->onDbChanged();
            *changed = false;
        }
     }
}
