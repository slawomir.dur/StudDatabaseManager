#include "table.h"

Table::Table(QWidget *parent) : QWidget(parent)
{
    table = new QTableWidget(12, 6, this);
    table->setFixedSize(900, 500);
    QStringList stringList;
    stringList.append("Imie");
    stringList.append("Nazwisko");
    stringList.append("Nr. telefonu");
    stringList.append("Adres e-mail");
    stringList.append("Ostatnie powiadomienie");
    stringList.append("Nastepne powiadomienie");
    table->setHorizontalHeaderLabels(stringList);
    table->setColumnWidth(4, 180);
    table->setColumnWidth(5, 190);
}
