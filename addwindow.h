#ifndef ADDWINDOW_H
#define ADDWINDOW_H

#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSqlTableModel>
#include <window.h>

class AddWindow : public QDialog
{
    Q_OBJECT
public:
    explicit AddWindow(QWidget *table = nullptr);
private:
    QWidget *upper_table;
    QHBoxLayout *nameBox;
    QHBoxLayout *surnameBox;
    QHBoxLayout *phoneBox;
    QHBoxLayout *mailBox;
    QHBoxLayout *buttons;
    QVBoxLayout *primeLayout;

    QLabel *title;
    QLabel *nameLabel;
    QLabel *surnameLabel;
    QLabel *phoneLabel;
    QLabel *mailLabel;
    QLineEdit *nameEdit;
    QLineEdit *surnameEdit;
    QLineEdit *phoneEdit;
    QLineEdit *mailEdit;
    QPushButton *add;
    QPushButton *cancel;
public slots:
    void on_add_button_clicked();
};

#endif // ADDWINDOW_H
