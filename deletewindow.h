#ifndef DELETEWINDOW_H
#define DELETEWINDOW_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QVBoxLayout>

class DeleteWindow : public QDialog
{
    Q_OBJECT
public:
    DeleteWindow(QWidget *table);
private:
    QWidget *upper_table;
    QHBoxLayout *nameLayout;
    QHBoxLayout *surnameLayout;
    QHBoxLayout *buttons;
    QVBoxLayout *primeLayout;
    QLabel *title;
    QLabel *nameLabel;
    QLabel *surnameLabel;
    QLineEdit *nameEdit;
    QLineEdit *surnameEdit;
    QPushButton *delButton;
    QPushButton *cancel;
private slots:
    void on_ok_clicked();
};

#endif // DELETEWINDOW_H
