#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include "buttongroup.h"
#include "dbtable.h"

class Window:public QWidget
{
    Q_OBJECT
public:
   explicit Window(QWidget *parent = nullptr);
   QSqlTableModel* getTableModel();
private:
    DbTable *tab;
    buttonGroup *buttons;

public slots:
    void onDbChanged();

};

#endif // WINDOW_H
