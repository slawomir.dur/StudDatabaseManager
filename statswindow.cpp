#include "statswindow.h"
#include <QHBoxLayout>
#include <QVBoxLayout>

statswindow::statswindow()
{
    statBox = new QGroupBox("Statystyki");
    clientCount = new QLabel("Liczba klientów:");
    notesSent = new QLabel("Wysłane powiadomienia:");
    closeButton = new QPushButton("Wyjdź");

    QHBoxLayout *clientsLayout = new QHBoxLayout;
    QHBoxLayout *notesLayout = new QHBoxLayout;

    QVBoxLayout *primeLayout = new QVBoxLayout;

    clientsLayout->addWidget(clientCount);
    QLabel *numClients = new QLabel("0");
    clientsLayout->addWidget(numClients);

    notesLayout->addWidget(notesSent);
    QLabel *numSent = new QLabel("0");
    notesLayout->addWidget(numSent);

    QVBoxLayout *innerBoxLayout = new QVBoxLayout;
    innerBoxLayout->addLayout(clientsLayout);
    innerBoxLayout->addLayout(notesLayout);
    innerBoxLayout->addWidget(closeButton);

    statBox->setLayout(innerBoxLayout);

    primeLayout->addWidget(statBox);
    setLayout(primeLayout);

    connect(closeButton, SIGNAL(clicked(bool)), this, SLOT(close()));
}
