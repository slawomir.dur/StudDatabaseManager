#include "dbtable.h"
#include <QHeaderView>
#include <fstream>
#include <cstdlib>
#include <ctime>
//#include <mutex>
//#include <condition_variable>
#include <QMutexLocker>

//std::mutex m;
extern QMutex m;
//extern std::condition_variable cv;
extern bool changed;



DbTable::DbTable(QWidget *parent) : QWidget(parent)
{
    db = new Database(this);
    db->connectToDb();

    QStringList headers;
    headers.append("ID");
    headers.append("IMIĘ");
    headers.append("NAZWISKO");
    headers.append("Nr. TELEFONU");
    headers.append("E-MAIL");
    headers.append("OSTATNIE POWIADOMIENIE");
    headers.append("NASTĘPNE POWIADOMIENIE");
    tableModel = new QSqlTableModel(this);
    tableModel->setTable("clients");

    for (int i = 0; i < tableModel->columnCount(); i++)
    {
        tableModel->setHeaderData(i, Qt::Horizontal, headers[i]);
    }

    tableView = new QTableView;
    tableView->setModel(tableModel);
    tableView->setColumnHidden(0, true);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    tableView->setSelectionMode(QAbstractItemView::SingleSelection);
    tableView->resizeColumnsToContents();
    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);


    tableModel->select();

    connect(db, SIGNAL(dataChanged()), this, SLOT(reactOnChange()));
}

QTableView* DbTable::getTable()
{
    return tableView;
}

QSqlTableModel* DbTable::getTableModel()
{
    return tableModel;
}

void DbTable::reactOnChange()
{
    qDebug() << "reactOnChange() called\n";
    emit DbChanged();

    srand(time(NULL));
    std::fstream f;
    f.open("checker.txt");
    if (f.is_open())
    {
        //std::unique_lock<std::mutex> lck{m};
        QMutexLocker locker(&m);
        f << rand()%100000;
        f.close();
        changed = true;
        //cv.notify_one();
    }
    else
        qDebug() << "can't create file\n";

    qDebug() << "DbChanged() emitted\n";
}
