#ifndef MAILWINDOW_H
#define MAILWINDOW_H

#include <QDialog>
#include <QPushButton>
#include <QLineEdit>
#include <QTextEdit>
#include <QLabel>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QHBoxLayout>

class MailWindow : public QDialog
{
    Q_OBJECT
public:
    MailWindow();
private:
    QLabel *title;
    QLabel *toLabel;
    QLineEdit *toEdit;
    QLabel *subjectLabel;
    QLineEdit *subjectEdit;
    QGroupBox *bodyBox;
    QTextEdit *bodyEdit;
    QPushButton *sendButton;
    QPushButton *cancel;

    QHBoxLayout *toLayout;
    QHBoxLayout *subjectLayout;
    QVBoxLayout *bodyLayout;
    QHBoxLayout *buttons;
    QVBoxLayout *primeLayout;
private slots:
    void on_send_button_clicked();
};

#endif // MAILWINDOW_H
