#include "deleteallwindow.h"
#include <QMessageBox>
#include <QVBoxLayout>
#include <QHBoxLayout>

DeleteAllWindow::DeleteAllWindow(QWidget *table)
{
    upper_table = table;

    question = new QLabel("Usunąć całą bazę danych?");
    delButton = new QPushButton("Usuń");
    cancelButton = new QPushButton("Anuluj");

    QHBoxLayout *buttonLayout = new QHBoxLayout;

    buttonLayout->addWidget(delButton);
    buttonLayout->addWidget(cancelButton);

    QVBoxLayout *primeLayout = new QVBoxLayout;
    primeLayout->addWidget(question);
    primeLayout->addLayout(buttonLayout);


    setLayout(primeLayout);

    connect(delButton, SIGNAL(clicked(bool)), this, SLOT(on_del_button_clicked()));
    connect(cancelButton, SIGNAL(clicked(bool)), this, SLOT(close()));
}

void DeleteAllWindow::on_del_button_clicked()
{
    Database db(upper_table);
    db.connectToDb();
    db.delAll();

    QMessageBox msg;
    msg.setText("Baza danych usunięta.");
    msg.setStandardButtons(QMessageBox::Ok);
    msg.exec();
    close();
}
