#include <QApplication>
#include "window.h"
//#include <fstream>
//#include <condition_variable>
#include "mythread.h"

//extern std::mutex m;
//std::condition_variable cv;

bool changed = false;

int main(int argc, char **argv)
{



    QApplication app(argc, argv);
    Window window;
    /*
    auto function = [&window]()->void
    {
        std::fstream f;
        int last = 0;
        int current = 0;

        while(true)
        {
            std::unique_lock<std::mutex> lck{m};
            //while (!changed)
                //cv.wait(lck);
            f.open("checker.txt");
            if (f.is_open())
            {
                last = current;
                f >> current;
                f.close();
            }
            if (last != current)
            {
                window.onDbChanged();
                changed = false;
            }
        }
    };
    */
    window.show();
    //std::thread t(function);
    MyThread thread;
    thread.setWin(&window);
    thread.setChan(&changed);
    thread.start();
    return app.exec();
    //t.join();
    thread.wait();
}

