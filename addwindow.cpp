#include "addwindow.h"
#include <QMessageBox>
#include "database.h"

AddWindow::AddWindow(QWidget *table)
{
    upper_table = table;
    nameBox = new QHBoxLayout;
    surnameBox = new QHBoxLayout;
    phoneBox = new QHBoxLayout;
    mailBox = new QHBoxLayout;

    buttons = new QHBoxLayout;

    primeLayout = new QVBoxLayout;

    title = new QLabel("Nowy klient");
    nameLabel = new QLabel("Imię:");
    surnameLabel = new QLabel("Nazwisko:");
    phoneLabel = new QLabel("Nr. telefonu:");
    mailLabel = new QLabel("Adres mailowy");
    nameEdit = new QLineEdit;
    surnameEdit = new QLineEdit;
    phoneEdit = new QLineEdit;
    mailEdit = new QLineEdit;
    add = new QPushButton("Dodaj");
    cancel = new QPushButton("Anuluj");

    nameBox->addWidget(nameLabel);
    nameBox->addWidget(nameEdit);

    surnameBox->addWidget(surnameLabel);
    surnameBox->addWidget(surnameEdit);

    phoneBox->addWidget(phoneLabel);
    phoneBox->addWidget(phoneEdit);

    mailBox->addWidget(mailLabel);
    mailBox->addWidget(mailEdit);

    buttons->addWidget(add);
    buttons->addWidget(cancel);

    primeLayout->addWidget(title);
    primeLayout->addLayout(nameBox);
    primeLayout->addLayout(surnameBox);
    primeLayout->addLayout(phoneBox);
    primeLayout->addLayout(mailBox);
    primeLayout->addLayout(buttons);

    this->setLayout(primeLayout);

    connect(add, SIGNAL(clicked(bool)), this, SLOT(on_add_button_clicked()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(close()));
}

void AddWindow::on_add_button_clicked()
{
    if (!(nameEdit->text() == "" || surnameEdit->text() == ""))
    {
        QVariantList data;
        data.append(nameEdit->text());
        data.append(surnameEdit->text());
        data.append(phoneEdit->text());
        data.append(mailEdit->text());

        Database db(upper_table);
        db.connectToDb();
        db.insert(data);
        //Database db(upper_table);
        //Database::connectToDb();
        //Database::insert(data);

        QMessageBox msg;
        msg.setText("Dodano klienta!");
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
        close();
    } else {
        QMessageBox msg;
        msg.setText("Brak imienia lub nazwiska!");
        msg.setStandardButtons(QMessageBox::Ok);
        msg.exec();
    }

}
