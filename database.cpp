#include "database.h"
#include "addwindow.h"

bool Database::open = false;
QSqlDatabase Database::db = QSqlDatabase::addDatabase("QSQLITE");

Database::Database(QWidget *table, QObject *parent) : QObject(parent)
{
    if (isOpen())
    {
        closeDb();
        open = false;
    }
    if (!(table == nullptr))
        connect(this, SIGNAL(dataChanged()), table, SLOT(reactOnChange()));
    db.setHostName("HOST");
    db.setDatabaseName("clients.db");
}

void Database::connectToDb()
{
    qDebug() << "trying to connect!\n";
    if(!QFile("clients.db").exists())
    {
        restoreDb();
    }
    else
    {
        //qDebug() << "isOpen(): " << isOpen() << "\n";
        if (isOpen())
            closeDb();
        openDb();
    }
}

bool Database::restoreDb()
{
    if (openDb())
    {
        if (!createTable())
        {
            return false;
        } else {
            return true;
        }
    } else {
        qDebug() << "Failed to restore db.\n";
        return false;
    }
}

bool Database::openDb()
{
    qDebug() << "Open db called\n";
    //db = QSqlDatabase::addDatabase("QSQLITE");
    //db.setHostName("HOST");
   // db.setDatabaseName("A:\\Praca\\Programming\\QtApps\\StudentsDatabaseManager\\clients.db");
    //db.setDatabaseName("clients.db");
    if (!isOpen())
    {
        if (db.open())
        {
            open = true;
            qDebug() << "Database open.\n";
            return true;
        }
        else return false;
    }
    return false;
}

void Database::closeDb()
{
    open = false;
    qDebug() << "Database closed.\n";
    db.close();
}

bool Database::createTable()
{
    QSqlQuery query;
    if (!query.exec("CREATE TABLE IF NOT EXISTS CLIENTS("
                    "id integer primary key AUTOINCREMENT,"
                    "name   VARCHAR(20) NOT NULL,"
                    "surname VARCHAR(20) NOT NULL,"
                    "phone VARCHAR(12),"
                    "mail VARCHAR(35),"
                    "last_note DATE,"
                    "next_note DATE)"))
    {
        qDebug() << "Create error.\n";
        qDebug() << query.lastError().text();
        return false;
    } else
    {
        //qDebug() << "Created table.\n";
        return true;
    }
}

bool Database::insert(const QVariantList &data)
{
    QSqlQuery query;
    query.prepare("INSERT INTO CLIENTS(name, surname, phone, mail) VALUES (:Name, :Surname, :Phone, :Mail)");
    query.bindValue(":Name", data[0].toString());
    query.bindValue(":Surname", data[1].toString());
    query.bindValue(":Phone", data[2].toString());
    query.bindValue(":Mail", data[3].toString());
    if (!query.exec())
    {
        qDebug() << "Insert failed.\n";
        return false;
    } else
    {
//        qDebug() << "Insert successful.\n";
        emit dataChanged();
        //qDebug() << "dataChanged() emitted\n";
        db.close();
        return true;
    }
}

bool  Database::delRecord(const QVariantList &data)
{
    QSqlQuery query;
    query.prepare("DELETE FROM CLIENTS WHERE NAME = :Name AND SURNAME = :Surname");
    query.bindValue(":Name", data[0].toString());
    query.bindValue(":Surname", data[1].toString());
    try {
        if (!query.exec())
        {
            throw -1;
        }
        else {
            emit dataChanged();
            //qDebug() << "dataChanged() emitted\n";
//            qDebug() << "Delete successful.\n";
            return true;
        }
    } catch (int) {
        qDebug() << "Delete failed.\n";
        return false;
    }
}

bool Database::delAll()
{
    QSqlQuery query;
    try {
        if (!query.exec("DELETE FROM CLIENTS"))
        {
            throw -1;
        }
        else {
            qDebug() << "Deleting all data successful.";
            db.close();
            emit dataChanged();
            return true;
        }
    } catch (int) {
        qDebug() << "Deleting all data unsuccessful.";
        return false;
    }
}

