#ifndef DBTABLE_H
#define DBTABLE_H

#include <QWidget>
#include <QSqlTableModel>
#include <QTableView>
#include "database.h"

class DbTable : public QWidget
{
    Q_OBJECT
public:
    explicit DbTable(QWidget *parent = nullptr);
    QTableView* getTable();
    QSqlTableModel* getTableModel();
private:
    Database *db;
    QSqlTableModel *tableModel;
    QTableView *tableView;
signals:
    void DbChanged();

public slots:
    void reactOnChange();
};

#endif // DBTABLE_H
