#include "window.h"
#include <QHBoxLayout>

Window::Window(QWidget *parent):QWidget(parent)
{
    setWindowTitle("Clients Database Manager");
    setFixedSize(980, 600);
    tab = new DbTable();
    buttons = new buttonGroup(this, tab);

    QHBoxLayout *hBoxLayout = new QHBoxLayout(this);
    hBoxLayout->addWidget(tab->getTable());
    hBoxLayout->addWidget(buttons);

    connect(tab, SIGNAL(DbChanged()), this, SLOT(onDbChanged()));
}

QSqlTableModel* Window::getTableModel()
{
    return tab->getTableModel();
}

void Window::onDbChanged()
{

    Database db;
    db.connectToDb();
    tab->getTableModel()->select();
    tab->getTable()->repaint();
    qDebug() << "onDbChanged() called\n";
}
