#ifndef TABLE_H
#define TABLE_H

#include <QWidget>
#include <QTableWidget>

class Table : public QWidget
{
    Q_OBJECT
public:
    explicit Table(QWidget *parent = nullptr);
private:
    QTableWidget *table;
signals:

public slots:
};

#endif // TABLE_H
