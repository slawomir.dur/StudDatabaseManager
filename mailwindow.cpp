#include "mailwindow.h"
#include <QMessageBox>
#include <QComboBox>

MailWindow::MailWindow()
{
    resize(650, 500);
    title = new QLabel("Wyślij wiadomość do klienta");
    toLabel = new QLabel("Do:");
    QComboBox *toComboBox = new QComboBox;
    toEdit = new QLineEdit;
    subjectLabel = new QLabel("Temat:");
    subjectEdit = new QLineEdit;
    bodyBox = new QGroupBox("Treść wiadmości");
    bodyEdit = new QTextEdit;
    sendButton = new QPushButton("Wyślij");
    cancel = new QPushButton("Anuluj");

    toLayout = new QHBoxLayout;
    subjectLayout = new QHBoxLayout;
    bodyLayout = new QVBoxLayout;
    buttons = new QHBoxLayout;
    primeLayout = new QVBoxLayout;

    toLayout->addWidget(toLabel);
    toLayout->addWidget(toComboBox);

    subjectLayout->addWidget(subjectLabel);
    subjectLayout->addWidget(subjectEdit);

    bodyLayout->addWidget(bodyEdit);
    bodyBox->setLayout(bodyLayout);

    buttons->addWidget(sendButton);
    buttons->addWidget(cancel);

    primeLayout->addWidget(title);
    primeLayout->addLayout(toLayout);
    primeLayout->addLayout(subjectLayout);
    primeLayout->addWidget(bodyBox);
    primeLayout->addLayout(buttons);

    this->setLayout(primeLayout);

    connect(sendButton, SIGNAL(clicked(bool)), this, SLOT(on_send_button_clicked()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(close()));
}


void MailWindow::on_send_button_clicked()
{
    if (!(toEdit->text() == "" || subjectEdit->text() == "" || bodyEdit->toPlainText() == ""))
    {
        QMessageBox msg;
        msg.setText("Wysłano wiadomość!");
        msg.exec();
        close();
    } else {
        QMessageBox msg;
        msg.setText("Pozostały puste pola!");
        msg.exec();
    }
}
