#ifndef DATABASE_H
#define DATABASE_H

#include <QtSql>
#include <QObject>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QDebug>


class Database : public QObject
{
    Q_OBJECT
public:
    Database(QWidget *table = nullptr, QObject *parent = nullptr);
    void connectToDb();
    bool insert(const QVariantList &data);
    bool delRecord(const QVariantList &data);
    bool delAll();
private:
    static QSqlDatabase db;
public:
    static bool open;
private:
    bool openDb();
    bool restoreDb();
    void closeDb();
    bool createTable();
    static bool isOpen() {return open;}
signals:
    void dataChanged();
};

#endif // DATABASE_H
