#ifndef BUTTONGROUP_H
#define BUTTONGROUP_H

#include <QPushButton>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QWidget>

class buttonGroup : public QWidget
{
    Q_OBJECT
public:
    explicit buttonGroup(QWidget *parent = nullptr, QWidget *table = nullptr);
private:
    QWidget *upper_table;
    QGroupBox *groupBox;
    QVBoxLayout *layout;
    QPushButton *add;
    QPushButton *del;
    QPushButton *showStats;
    QPushButton *delAll;
    QPushButton *quit;
    QPushButton *manualSend;
    QPushButton *options;

public slots:
    void on_add_button_clicked();
    void on_delete_button_clicked();
    void on_stats_button_clicked();
    void on_deleteAll_button_clicked();
    void on_manualSend_button_clicked();
    void on_quit_button_clicked();
};

#endif // BUTTONGROUP_H
