TEMPLATE = app
TARGET = ClientsDatabaseManager

QT = core gui
QT += sql
greaterThan(QT_MAJOR_VERSION, 4):QT += widgets

SOURCES += \
    studentsdatabasemanager.cpp \
    window.cpp \
    table.cpp \
    buttongroup.cpp \
    addwindow.cpp \
    deletewindow.cpp \
    mailwindow.cpp \
    database.cpp \
    dbtable.cpp \
    statswindow.cpp \
    deleteallwindow.cpp \
    mythread.cpp

HEADERS += \
    window.h \
    table.h \
    buttongroup.h \
    addwindow.h \
    deletewindow.h \
    mailwindow.h \
    database.h \
    dbtable.h \
    statswindow.h \
    deleteallwindow.h \
    mythread.h
