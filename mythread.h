#ifndef MYTHREAD_H
#define MYTHREAD_H
#include <QThread>
#include "window.h"

class MyThread : public QThread
{
    Q_OBJECT
private:
    void run();
private:
    Window *window;
    bool *changed;
public:
    void setWin(Window *win) {window = win;}
    void setChan(bool *chan) {changed = chan;}
};

#endif // MYTHREAD_H
