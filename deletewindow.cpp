#include "deletewindow.h"
#include <QMessageBox>
#include <QDebug>
#include "database.h"

DeleteWindow::DeleteWindow(QWidget *table)
{
    upper_table = table;
    nameLayout = new QHBoxLayout;
    surnameLayout = new QHBoxLayout;
    buttons = new QHBoxLayout;
    primeLayout = new QVBoxLayout;

    title = new QLabel("Usuń klienta");
    nameLabel = new QLabel("Imię");
    nameEdit = new QLineEdit;
    surnameLabel = new QLabel("Nazwisko");
    surnameEdit = new QLineEdit;
    delButton = new QPushButton("Usuń");
    cancel = new QPushButton("Anuluj");

    nameLayout->addWidget(nameLabel);
    nameLayout->addWidget(nameEdit);

    surnameLayout->addWidget(surnameLabel);
    surnameLayout->addWidget(surnameEdit);

    buttons->addWidget(delButton);
    buttons->addWidget(cancel);

    primeLayout->addWidget(title);
    primeLayout->addLayout(nameLayout);
    primeLayout->addLayout(surnameLayout);
    primeLayout->addLayout(buttons);

    this->setLayout(primeLayout);

    connect(delButton, SIGNAL(clicked(bool)), this, SLOT(on_ok_clicked()));
    connect(cancel, SIGNAL(clicked(bool)), this, SLOT(close()));
}


void DeleteWindow::on_ok_clicked()
{
    if (!(nameEdit->text() == "" || surnameEdit->text() == ""))
    {
        QMessageBox msg;
        msg.setText("Na pewno chcesz usunąć klienta?");
        msg.setInformativeText("To spowoduje bezpowrotną utratę jego danych");
        msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msg.setDefaultButton(QMessageBox::No);
        int choice = msg.exec();

        QVariantList data;
        Database db(upper_table);

        QMessageBox msg2;
        msg2.setText("Usunięto klienta!");
        msg2.setStandardButtons(QMessageBox::Ok);

        switch(choice)
        {
            case QMessageBox::Yes:
                qDebug() << "Deleting operation here\n";
                data.append(nameEdit->text());
                data.append(surnameEdit->text());
                db.delRecord(data);

                msg2.exec();
                qDebug() << "Before close()\n";
                close();
                qDebug() << "After close()\n";
            break;
            case QMessageBox::No:
                qDebug() << "Cancelling operation.\n";
                close();
            break;
            default:
                qDebug() << "This should never be displayed\n";
            break;
        }
    } else {
        QMessageBox msg;
        msg.setText("Przynajmniej jedno pole jest puste!");
        msg.setStandardButtons(QMessageBox::Ok);
        msg.setDefaultButton(QMessageBox::Ok);
        msg.exec();
    }

}


