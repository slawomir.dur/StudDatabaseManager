#ifndef DELETEALLWINDOW_H
#define DELETEALLWINDOW_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <database.h>

class DeleteAllWindow : public QDialog
{
    Q_OBJECT
public:
    DeleteAllWindow(QWidget *table);
private:
    QWidget *upper_table;
    QLabel *question;
    QPushButton *delButton;
    QPushButton *cancelButton;
public slots:
    void on_del_button_clicked();
};

#endif // DELETEALLWINDOW_H
