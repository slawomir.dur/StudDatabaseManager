#include "buttongroup.h"
#include "addwindow.h"
#include "deletewindow.h"
#include "deleteallwindow.h"
#include "statswindow.h"
#include "mailwindow.h"
#include <QApplication>
#include <QMessageBox>

buttonGroup::buttonGroup(QWidget *parent, QWidget *table) : QWidget(parent)
{
    upper_table = table;
    setFixedSize(150, 300);
    QGroupBox *groupBox = new QGroupBox("Dostępne akcje", this);
    QVBoxLayout *layout = new QVBoxLayout;
    add = new QPushButton("Dodaj");
    del = new QPushButton("Usuń");
    showStats = new QPushButton("Pokaż statystyki");
    delAll = new QPushButton("Usuń wszystkich");
    manualSend = new QPushButton("Powiadom");
    options = new QPushButton("Opcje");
    quit = new QPushButton("Wyjdź");

    layout->addWidget(add);
    layout->addWidget(showStats);
    layout->addWidget(del);
    layout->addWidget(delAll);
    layout->addWidget(manualSend);
    layout->addWidget(options);
    layout->addWidget(quit);

    groupBox->setLayout(layout);

    connect(add, SIGNAL(clicked(bool)), this, SLOT(on_add_button_clicked()));
    connect(del, SIGNAL(clicked(bool)), this, SLOT(on_delete_button_clicked()));
    connect(showStats, SIGNAL(clicked(bool)), this, SLOT(on_stats_button_clicked()));
    connect(delAll, SIGNAL(clicked(bool)), this, SLOT(on_deleteAll_button_clicked()));
    connect(manualSend, SIGNAL(clicked(bool)), this, SLOT(on_manualSend_button_clicked()));

    connect(quit, SIGNAL(clicked(bool)), this, SLOT(on_quit_button_clicked()));

}


void buttonGroup::on_add_button_clicked()
{
    AddWindow addwindow(upper_table);
    addwindow.setModal(true);
    addwindow.exec();
}

void buttonGroup::on_delete_button_clicked()
{
    DeleteWindow delWindow(upper_table);
    delWindow.setModal(true);
    delWindow.exec();
}

void buttonGroup::on_stats_button_clicked()
{
    statswindow statwindow;
    statwindow.setModal(true);
    statwindow.exec();
}

void buttonGroup::on_manualSend_button_clicked()
{
    MailWindow mailWindow;
    mailWindow.setModal(true);
    mailWindow.exec();
}

void buttonGroup::on_deleteAll_button_clicked()
{
    DeleteAllWindow delallwindow(upper_table);
    delallwindow.setModal(true);
    delallwindow.exec();
}

void buttonGroup::on_quit_button_clicked()
{
    QMessageBox msg;
    msg.setText("Na pewno wyjść?");
    msg.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    int choice = msg.exec();
    switch(choice)
    {
        case QMessageBox::Yes:
            QApplication::instance()->quit();
        break;
        case QMessageBox::No:
            msg.close();
        break;
        default:
        break;
    }
}
