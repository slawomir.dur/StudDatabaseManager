#ifndef STATSWINDOW_H
#define STATSWINDOW_H

#include <QDialog>
#include <QLabel>
#include <QGroupBox>
#include <QPushButton>


class statswindow : public QDialog
{
    Q_OBJECT
public:
    statswindow();
private:
    QGroupBox *statBox;
    QLabel *clientCount;
    QLabel *notesSent;
    QPushButton *closeButton;
};

#endif // STATSWINDOW_H
